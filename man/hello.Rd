\name{hello}
\alias{hello}
\title{
Hello OpenMP example
}
\description{
Prints \emph{Hello World} and returns results of a parallel loop with counts of each thread involved.
}
\usage{
hello()
}
\value{
Integer vector of length of number of available threads.
}
\examples{
hello()
}
