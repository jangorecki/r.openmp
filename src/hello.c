#include <Rinternals.h>
#include <omp.h>

SEXP Chello() {
  Rprintf("Hello OpenMP\n");

  int threads = omp_get_max_threads();
  SEXP Rth = PROTECT(allocVector(INTSXP, threads));
  int *th = INTEGER(Rth);
  for (int i=0; i<threads; ++i)
    th[i] = 0;

  #pragma omp parallel for
  for (int i=0; i<threads*1000; ++i) {
    int t = omp_get_thread_num();
    th[t]++;
  }

  UNPROTECT(1);
  return Rth;
}
